function aggregateField(theObject, first, second) {
    for (var i = first; i <= second; ++i) {
        let inner = document.createElement('li');
        inner.textContent = String(i);
        theObject.appendChild(inner);
    }
}

window.onload = function() {
    document.getElementById("filter").classList.add("open");
    if (window.matchMedia("(min-width: 992px)").matches) {
        document.getElementById("filter").classList.add("open");
    } else {
        document.getElementById("filter").classList.remove("open");
    }

    window.onresize = function() {
        if (window.matchMedia("(min-width: 992px)").matches) {
            document.getElementById("filter").classList.add("open");
        } else {
            document.getElementById("filter").classList.remove("open");
        }
    }
    document.getElementById("btn-open-filter").onclick = function() {
        document.getElementById("filter").classList.toggle('open');
    }

    var datePanels = document.querySelectorAll('.date-panel');
    for (var i = 0; i < datePanels.length; ++i) {
        var yearsUl = datePanels[i].querySelector('.year').querySelector('.dropdown-menu');
        var monthUl = datePanels[i].querySelector('.month').querySelector('.dropdown-menu');
        var dayUl = datePanels[i].querySelector('.day').querySelector('.dropdown-menu');
        var now = new Date();
        aggregateField(yearsUl, 1980, now.getFullYear());
        aggregateField(monthUl, 1, 12);
        aggregateField(dayUl, 1, 31);
    }

    var datePanels = document.querySelectorAll('.date-panel');
    for (var i = 0; i < datePanels.length; ++i) {
        datePanels[i].onclick = function(event) {
            let target = event.target;
            if (target.tagName != 'LI') return;
            let parent = target.parentNode.parentNode;
            parent.querySelector('a').querySelector('span').textContent = target.textContent;
        };
    }

}